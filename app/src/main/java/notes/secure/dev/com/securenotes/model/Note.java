package notes.secure.dev.com.securenotes.model;

import android.support.annotation.NonNull;

/**
 */

public  class Note extends  Item implements Comparable<Note>  {

    private String content;

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    enum FILE_TYPE{
        nonsharable,
        sharable;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    private String name;
    private String uri;


    @Override
    public boolean equals(Object obj) {

        if(obj instanceof Note){
            Note note =(Note) obj;
            return note.getName().equals(getName());
        }
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }

    @Override
    public int compareTo(@NonNull Note o) {

        if(o instanceof Note){
            Note note = o;
            return note.getName().compareTo(getName());
        }
        return 0;
    }
}
