package notes.secure.dev.com.securenotes;

public final class Constants {

    public static final String ARG_FOLDER_ID ="_id_folder" ;
    public static final String ARG_FOLDER_NAME = "_name_folder";

    public static final String ARG_TAB_ID ="_id_tab" ;
    public static final String ARG_TAB_NAME = "_name_tab";

    public static final int SHOW_TAB = 1;
    public static final String ARG_NUM_TABS = "num_tabs";

    public static final String ARG_NOTE_NAME = "_name_note";
    public static final String ARG_NOTE_URI = "_name_uri";


}
