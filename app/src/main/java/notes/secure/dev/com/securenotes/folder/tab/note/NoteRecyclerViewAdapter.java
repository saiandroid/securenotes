package notes.secure.dev.com.securenotes.folder.tab.note;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import notes.secure.dev.com.securenotes.R;
import notes.secure.dev.com.securenotes.api.data.DataApi;
import notes.secure.dev.com.securenotes.api.data.NotesDataApi;
import notes.secure.dev.com.securenotes.folder.tab.TabParentActivity;
import notes.secure.dev.com.securenotes.model.Folder;
import notes.secure.dev.com.securenotes.model.Note;
import notes.secure.dev.com.securenotes.model.Tab;


/**
 * {@link RecyclerView.Adapter} that can display a {@link Folder} and makes a call to the
 * specified {@link NoteRecyclerViewAdapter}.
 * TODO: Replace the implementation with code for your data type.
 */
public class NoteRecyclerViewAdapter extends RecyclerView.Adapter<NoteRecyclerViewAdapter.ViewHolder> {

    private final List<Note> mNotes;
    private final Context mcontext;
    private DataApi mDataApi;
    private NoteInteractionListener mNoteInteractionListener;

    public NoteRecyclerViewAdapter(Context context,List<Note> items,NoteInteractionListener noteInteractionListener) {
        mNotes = items;
        mNoteInteractionListener =noteInteractionListener;
        mDataApi = NotesDataApi.newInstance(context);
        mcontext = context;
    }
    private void showMessage(String s) {
        Toast.makeText(mcontext,s,Toast.LENGTH_LONG).show();
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.note_row, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mNote = mNotes.get(position);
        holder.mContentView.setText(mNotes.get(position).getName());
        holder.mView.setTag(holder.mNote);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Note note = (Note) v.getTag();
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                if(mNoteInteractionListener!=null){
                    mNoteInteractionListener.onNoteOpened(note);
                }

            }
        });
        holder.deleteNote.setTag(holder.mNote);
        holder.deleteNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Note note = (Note) view.getTag();
                showNoteDeleteConfirmDialog(mcontext,"?",note);

            }
        });

        holder.editNote.setTag(holder.mNote);
        holder.editNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Note note = (Note) view.getTag();

                showEditNoteDialog(note);

            }
        });
    }

    public void showEditNoteDialog(final Note note) {

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(mcontext);
        View promptsView = li.inflate(R.layout.name_prompt_dialog, null);
        final TextView nameTextView = promptsView.findViewById(R.id.textView1);
        nameTextView.setText("Enter new name");

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                mcontext);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = promptsView
                .findViewById(R.id.editTextDialogUserInput);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {

                                // get user input and set it to result
                                // edit text
                                if (userInput.getText().toString().trim().length() == 0) {
                                    showMessage("Enter a name");
                                    return;

                                }
                              Note newNote =   mDataApi.updateNote(mcontext, new DataApi.DataApiCallBack() {
                                    @Override
                                    public void onError(String error, Exception e) {
                                        showMessage(error);

                                    }
                                },note.getUri(),userInput.getText().toString(),note.getName());
                                if(newNote!=null){
                                   int ind =  mNotes.indexOf(note);
                                   mNotes.add(ind,newNote);
                                   mNotes.remove(note);
                                   notifyDataSetChanged();
                                    showMessage("Note renamed successfully");
                                    mNoteInteractionListener.onNoteEdit(note);

                                }


                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public boolean addItem(Note note){
        mNotes.add(note);
        notifyItemInserted(mNotes.size());
        return true;
    }
    public boolean removeItem(Note note){
        int ind = mNotes.indexOf(note);
        mNotes.remove(ind);
        notifyItemRemoved(ind);
        return true;
    }
    public void removeAllItems(){
        mNotes.clear();
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return mNotes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final View editNote,deleteNote;
        public final TextView mContentView;
        public Note mNote;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mContentView =  view.findViewById(R.id.content);
            editNote =view.findViewById(R.id.editNote);
            deleteNote =view.findViewById(R.id.deleteNote);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }

    public void showNoteDeleteConfirmDialog(Context context, String message, final Note note){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);
        alertDialogBuilder
                .setCancelable(false)
                .setTitle("Delete Note "+message)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {

                                boolean deleted =   mDataApi.deleteNote(mcontext, new DataApi.DataApiCallBack() {
                                    @Override
                                    public void onError(String error, Exception e) {
                                        showMessage(error);

                                    }
                                },note.getUri());

                                if(deleted){
                                    showMessage("Note deleted successfully");
                                    mNoteInteractionListener.onNoteDelete(note);

                                }

                                // get user input and set it to result
                                // edit text


                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }


}
