package notes.secure.dev.com.securenotes;

import android.util.Base64;

import java.security.Key;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import notes.secure.dev.com.securenotes.api.data.DataApi;

public class Crypto {

    public static final String [] ALGOS ={"AES","DES","DESede"};
    public static final String [] ALGOS_KEYS ={"1234567890123456","12345678","1234567890123456"};
    public static final int ALGO_INDEX=1;




    public static String encryptContent(String algorithum, String key, String content, DataApi.DataApiCallBack dataApiCallBack) throws  Exception{
        try {
            Key secretKey = new SecretKeySpec(key.getBytes(), algorithum);
            Cipher cipher = Cipher.getInstance(algorithum);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);

            byte[] inputBytes = content.getBytes("UTF-8");
            byte[] outputBytes = cipher.doFinal(inputBytes);
            String s  = Base64.encodeToString(outputBytes,Base64.DEFAULT);


//            String s = new String(outputBytes);
            return s;


        } catch (Exception e) {
            e.printStackTrace();
            dataApiCallBack.onError(e.toString(),e);

        }
        return null;
    }

   public static String decryptContent( String algorithum, String key, String content, DataApi.DataApiCallBack dataApiCallBack) throws  Exception{
        try {
            Key secretKey = new SecretKeySpec(key.getBytes(), algorithum);
            Cipher cipher = Cipher.getInstance(algorithum);
            cipher.init(Cipher.DECRYPT_MODE, secretKey);



            byte[] inputBytes = Base64.decode(content,Base64.DEFAULT);
            byte[] outputBytes = cipher.doFinal(inputBytes);
//            byte[]  outputBytes =Base64.decode(inputBytes,Base64.DEFAULT);
//            outputBytes = cipher.doFinal(inputBytes);
            String s = new String(outputBytes,"UTF-8");
            return s;


        } catch (Exception  e) {
            e.printStackTrace();
            dataApiCallBack.onError(e.toString(),e);

        }
        return null;
    }


}
