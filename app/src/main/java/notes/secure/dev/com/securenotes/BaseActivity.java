package notes.secure.dev.com.securenotes;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import notes.secure.dev.com.securenotes.api.data.DataApi;
import notes.secure.dev.com.securenotes.api.data.NotesDataApi;

public abstract class BaseActivity extends AppCompatActivity {
    public DataApi mDataApi;

    public BaseActivity(){

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDataApi = NotesDataApi.newInstance(this);
    }

    public void showMessage(String error) {
        Toast.makeText(this,error,Toast.LENGTH_LONG).show();
    }

    public void showCreateFolderDialog() {

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.name_prompt_dialog, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = promptsView
                .findViewById(R.id.editTextDialogUserInput);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {

                                // get user input and set it to result
                                // edit text
                                if (userInput.getText().toString().trim().length() == 0) {
                                    showMessage("Enter a name");
                                    return;

                                }
                                    mDataApi.createRootFolder(BaseActivity.this, new DataApi.DataApiCallBack() {
                                        @Override
                                        public void onError(String error, Exception e) {
                                            showMessage(error);

                                        }
                                    }, userInput.getText().toString().trim());
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


}
