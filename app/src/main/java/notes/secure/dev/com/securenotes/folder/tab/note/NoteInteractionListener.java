package notes.secure.dev.com.securenotes.folder.tab.note;

import notes.secure.dev.com.securenotes.model.Note;

public interface NoteInteractionListener {

    void onNoteEdit(Note note);
    void onNoteDelete(Note note);
    void onNoteOpened(Note note);
    void onNoteUiClosed(Note note);


}
