package notes.secure.dev.com.securenotes;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import notes.secure.dev.com.securenotes.api.data.NotesDataApi;
import notes.secure.dev.com.securenotes.folder.FolderFragment;
import notes.secure.dev.com.securenotes.folder.tab.TabParentActivity;
import notes.secure.dev.com.securenotes.model.Folder;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static notes.secure.dev.com.securenotes.Constants.*;

public class MainActivity extends BaseActivity implements FolderFragment.OnListFragmentInteractionListener {
    private static final int REQUEST_WRITE_STORAGE = 1;



//    RecyclerView mFolderRecyclerView;
  FloatingActionButton fab;
  FolderFragment mFolderFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


//        mFolderRecyclerView = findViewById(R.id.folderRecyclerView);


        fab = (FloatingActionButton) findViewById(R.id.add_folder);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                showCreateFolderDialog();
                mFolderFragment.showCreateFolderDialog();
            }
        });



        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment  =fragmentManager.findFragmentByTag(FolderFragment.class.getSimpleName());
        if(fragment instanceof FolderFragment){
            mFolderFragment = (FolderFragment) fragment;
        }
        if(mFolderFragment ==null) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            mFolderFragment = FolderFragment.newInstance(1);
            fragmentTransaction.add(R.id.fragment_container, mFolderFragment, FolderFragment.class.getSimpleName());
            fragmentTransaction.commit();
        }

        if(NotesDataApi.useExternal ==true) {

            mayRequestExternalStorage();
        }

    }

    @Override
    protected void onRestart() {
        super.onRestart();

       Fragment fragment =  getSupportFragmentManager().findFragmentByTag(FolderFragment.class.getSimpleName());
       if(fragment instanceof  FolderFragment){
           FolderFragment folderFragment = (FolderFragment) fragment;
           folderFragment.init();

       }

    }

    private boolean mayRequestExternalStorage() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) {
            showMessage(getString(R.string.permission_rationale));
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);


        } else {
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
        }
        return false;
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListFragmentInteraction(Folder folder) {
   if(folder!=null){

           Intent intent = new Intent(this, TabParentActivity.class);
           intent.putExtra(ARG_FOLDER_ID,folder.getFolderId());
           intent.putExtra(ARG_FOLDER_NAME,folder.getName());
           startActivityForResult(intent,SHOW_TAB);

   }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode ==SHOW_TAB){
            if(resultCode ==RESULT_OK){
                if(data.hasExtra(ARG_NUM_TABS)){
                    int numtabs = data.getIntExtra(ARG_NUM_TABS,0);
                }
            }
        }
    }
}
