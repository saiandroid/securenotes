package notes.secure.dev.com.securenotes.folder;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import notes.secure.dev.com.securenotes.R;
import notes.secure.dev.com.securenotes.api.data.DataApi;
import notes.secure.dev.com.securenotes.api.data.NotesDataApi;
import notes.secure.dev.com.securenotes.model.Folder;
import notes.secure.dev.com.securenotes.model.Tab;

import static notes.secure.dev.com.securenotes.Constants.ARG_NUM_TABS;
import static notes.secure.dev.com.securenotes.Constants.SHOW_TAB;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class FolderFragment extends Fragment  implements FolderRecyclerViewAdapter.OnDataModifiedListener {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private DataApi mDataApi;

    List<Folder> mFolderList;



    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public FolderFragment() {
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode ==SHOW_TAB){
            if(resultCode == Activity.RESULT_OK){
                if(data.hasExtra(ARG_NUM_TABS)){
                    int numtabs = data.getIntExtra(ARG_NUM_TABS,0);
                }
            }
        }
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static FolderFragment newInstance(int columnCount) {
        FolderFragment fragment = new FolderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
        mDataApi = NotesDataApi.newInstance(getContext());

        init();

        setHasOptionsMenu(true);

    }

    public void init() {
        final Iterable<Folder>  folderIterable =    mDataApi.getRootFolders(new DataApi.DataApiCallBack() {

            @Override
            public void onError(String error, Exception e) {

                showError(error,e);

            }
        });
        mFolderList =new ArrayList<>();

        if(folderIterable!=null ) {
           Iterator<Folder> folderIterator=  folderIterable.iterator();
            while (folderIterator.hasNext()) {
                Folder folder =folderIterator.next();
                mFolderList.add(folder);

              Iterable<Tab> tabIterable =    mDataApi.getTabsOfFolder(new DataApi.DataApiCallBack() {
                    @Override
                    public void onError(String error, Exception e) {

                    }
                },folder);
              if(tabIterable!=null){
                  Iterator<Tab> tabIterator = tabIterable.iterator();
                  List<Tab> tabList = new ArrayList<>();
                  while(tabIterator.hasNext()){
                      tabList.add(tabIterator.next());
                  }
                  folder.setTabList(tabList);
              }

            }

        }
        if(mFolderRecyclerViewAdapter==null) {

            mFolderRecyclerViewAdapter = new FolderRecyclerViewAdapter(getContext()
                    , mFolderList, mListener, this);
        }else{
            mFolderRecyclerViewAdapter.setFolderList(mFolderList);
            mFolderRecyclerViewAdapter.notifyDataSetChanged();

            if(mFolderRecyclerViewAdapter.getItemCount()>0){
                mNoFolderTextView.setVisibility(View.GONE);
            }else{
                mNoFolderTextView.setVisibility(View.VISIBLE);
            }
        }


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {


//        inflater.inflate(R.menu.menu_folder, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.action_add_folder){
          showCreateFolderDialog();

        }
        return true;
    }

    private void showError(String error, Exception e) {
        if(error!=null){
            Toast.makeText(getContext(),error,Toast.LENGTH_LONG).show();
        }
    }

    private FolderRecyclerViewAdapter mFolderRecyclerViewAdapter;
    private TextView mNoFolderTextView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_folder, container, false);

        mNoFolderTextView = view.findViewById(R.id.noFolder);
        ((AppCompatActivity)  getActivity()).getSupportActionBar().setTitle("Folders");

        // Set the adapter
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list);
            DividerItemDecoration horizontalDecoration = new DividerItemDecoration(recyclerView.getContext(),
                    DividerItemDecoration.VERTICAL);
            Drawable horizontalDivider = ContextCompat.getDrawable(getActivity(), R.drawable.horizontal_divider);
            horizontalDecoration.setDrawable(horizontalDivider);
            recyclerView.addItemDecoration(horizontalDecoration);

            DividerItemDecoration verticalDecoration = new DividerItemDecoration(recyclerView.getContext(),
                    DividerItemDecoration.HORIZONTAL);
            Drawable verticalDivider = ContextCompat.getDrawable(getActivity(), R.drawable.vertical_divider);
            verticalDecoration.setDrawable(verticalDivider);
//            recyclerView.addItemDecoration(verticalDecoration);

            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            recyclerView.setAdapter(mFolderRecyclerViewAdapter);
            if(mFolderRecyclerViewAdapter.getItemCount()>0){
                mNoFolderTextView.setVisibility(View.GONE);
            }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void updateUi() {
        init();

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Folder item);

//        void onFolderDeleted(Folder item);
    }
    public void showError(String error) {
        Toast.makeText(getContext(),error,Toast.LENGTH_SHORT).show();
    }

    public void showConfirmDialog(Context context, String message){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);
        alertDialogBuilder
                .setCancelable(false)
                .setTitle("Delete Folder "+message)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {

                                // get user input and set it to result
                                // edit text


                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }
    public void showCreateFolderDialog() {

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(getContext());
        View promptsView = li.inflate(R.layout.name_prompt_dialog, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getContext());

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = promptsView
                .findViewById(R.id.editTextDialogUserInput);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {

                                // get user input and set it to result
                                // edit text
                                if (userInput.getText().toString().trim().length() == 0) {
                                    showError("Enter a name");
                                    return;

                                }
                               final Folder folder =  mDataApi.createRootFolder(getContext(), new DataApi.DataApiCallBack() {
                                    @Override
                                    public void onError(String error, Exception e) { {

                                            showError(error);
                                        }



                                    }
                                }, userInput.getText().toString().trim());

                                if(folder!=null){
                                    mFolderList.add(folder);
                                    mFolderRecyclerViewAdapter.notifyDataSetChanged();
                                    if(mFolderRecyclerViewAdapter.getItemCount()>0){
                                        mNoFolderTextView.setVisibility(View.GONE);
                                    }

                                }
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }
}
