package notes.secure.dev.com.securenotes.folder.tab;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import notes.secure.dev.com.securenotes.BaseActivity;
import notes.secure.dev.com.securenotes.R;
import notes.secure.dev.com.securenotes.api.data.DataApi;
import notes.secure.dev.com.securenotes.api.data.NotesDataApi;
import notes.secure.dev.com.securenotes.folder.FolderFragment;
import notes.secure.dev.com.securenotes.folder.tab.note.NoteFragment;
import notes.secure.dev.com.securenotes.folder.tab.note.NoteInteractionListener;
import notes.secure.dev.com.securenotes.folder.tab.note.NoteRecyclerViewAdapter;
import notes.secure.dev.com.securenotes.model.Folder;
import notes.secure.dev.com.securenotes.model.Note;
import notes.secure.dev.com.securenotes.model.Tab;
import static notes.secure.dev.com.securenotes.Constants.*;
import static notes.secure.dev.com.securenotes.Constants.ARG_TAB_ID;
import static notes.secure.dev.com.securenotes.Constants.ARG_TAB_NAME;


public class TabParentActivity extends BaseActivity implements  NoteInteractionListener{

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private String folderName;
    private long folderId;
    private List<Tab> mTabList;
    Folder parentFolder;
    private TabLayout tabLayout;
    private int currentPage;
    private TextView noTabsTextView;
    private  FloatingActionButton mFab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_parent);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        tabLayout =findViewById(R.id.tabs);
        noTabsTextView = findViewById(R.id.notabs);


       if(getIntent().hasExtra(ARG_FOLDER_NAME) && getIntent().hasExtra(ARG_FOLDER_ID)) {
           folderName = getIntent().getStringExtra(ARG_FOLDER_NAME);
           folderId = getIntent().getLongExtra(ARG_FOLDER_ID, 0);
           parentFolder = new Folder(folderId,folderName);
       }else{
           //
           showMessage(DataApi.ErrorMessages.ERROR_FOLDER_DOES_NOT_EXIST);
           finish();
           return;
       }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(folderName);

        mTabList = new ArrayList<>();
         Iterable<Tab> iterable = mDataApi.getTabsOfFolder(new DataApi.DataApiCallBack() {
           @Override
           public void onError(String error, Exception e) {
             showMessage(error);
           }
       },parentFolder);
         if(iterable!=null){
             Iterator<Tab> tabIterator = iterable.iterator();
             while(tabIterator.hasNext()){
                 mTabList.add(tabIterator.next());
             }
         }

         if(mTabList.size()==0){
             noTabsTextView.setVisibility(View.VISIBLE);
         }


        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),mTabList);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabLayout.setupWithViewPager(mViewPager);

        mFab = (FloatingActionButton) findViewById(R.id.fab);

        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mViewPager.getCurrentItem();
               if( mSectionsPagerAdapter.getCount()>currentPage) {

                   showCreateNoteDialog(mSectionsPagerAdapter.getTab(currentPage));
               }else{
                   showMessage("Please create tab first from add menu in action bar");
               }

            }
        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
                mCurrentFragment = (TabFragment) mSectionsPagerAdapter.getRegisteredFragment(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });



    }



    @Override
    protected void onRestart() {
        super.onRestart();
    }




    TabFragment mCurrentFragment;
    SparseArray<TabFragment> registeredFragments = new SparseArray<>();
    public void showCreateNoteDialog(final Tab tab) {

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.name_prompt_dialog, null);
        TextView nameTextView = promptsView.findViewById(R.id.textView1);
        nameTextView.setText("Enter new note name.");

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = promptsView
                .findViewById(R.id.editTextDialogUserInput);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {

                                // get user input and set it to result
                                // edit text
                                if (userInput.getText().toString().trim().length() == 0) {
                                    showMessage("Enter a name");
                                    return;

                                }
                               Note note = mDataApi.createNote(TabParentActivity.this, new DataApi.DataApiCallBack() {
                                    @Override
                                    public void onError(String error, Exception e) {
                                        showMessage(error);

                                    }
                                },tab, userInput.getText().toString().trim());
                                if(note!=null){
//                                    showMessage("Note "+note.getName()+" created successfully");
                                    if(mTabList.size()==1){
                                        mCurrentFragment = (TabFragment) mSectionsPagerAdapter.getRegisteredFragment(currentPage);
                                    }
                                    if(mCurrentFragment.addNote(note)){
                                        showMessage("Note "+note.getName()+" created successfully");

                                    }else {
                                        showMessage("Note not added");

                                    }

                                }else{
                                    showMessage("Note not added");
                                }
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tab_parent, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add_tab) {

            showCreateTabDialog(parentFolder,  new DataApi.DataApiCallBack() {
                @Override
                public void onError(String error, Exception e) {
                    showMessage(error);

                }
            });
            return true;

        }else if(id ==R.id.action_delete_tab){
            if(mSectionsPagerAdapter.getCount()>0) {

                showConfirmDeleteDialog(this, ",All notes inside will be deleted ?", parentFolder, mSectionsPagerAdapter.getTab(currentPage));
            }else{
                showMessage("No tabs to delete");
            }
        }else if(id ==R.id.action_edit_tab){
            if(mSectionsPagerAdapter.getCount()>0) {

                showEditTabDialog( mSectionsPagerAdapter.getTab(currentPage));
            }else{
                showMessage("No tabs to delete");
            }
        }
//        case android.R.id.home:
//        NavUtils.navigateUpFromSameTask(this);
//        return true;

        return super.onOptionsItemSelected(item);
    }

    public void showEditTabDialog(final Tab oldTab) {

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.name_prompt_dialog, null);
        final TextView nameTextView = promptsView.findViewById(R.id.textView1);
        nameTextView.setText("Enter new name");

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = promptsView
                .findViewById(R.id.editTextDialogUserInput);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {

                                // get user input and set it to result
                                // edit text
                                if (userInput.getText().toString().trim().length() == 0) {
                                    showMessage("Enter a name");
                                    return;

                                }
                                Tab tab =   mDataApi.updateTab(TabParentActivity.this, new DataApi.DataApiCallBack() {
                                    @Override
                                    public void onError(String error, Exception e) {
                                        showMessage(error);

                                    }
                                },parentFolder,userInput.getText().toString(),oldTab.getName());
                                if(tab!=null){
                                    int ind =  mTabList.indexOf(oldTab);


                                    mTabList.add(ind,tab);
                                    mTabList.remove(oldTab);
                                    showMessage("Tab renamed successfully");
                                    registeredFragments.get(currentPage).reFillNotes(tab);
                                    mSectionsPagerAdapter.notifyDataSetChanged();
//                                    fillTabData(folder);
//                                    notifyDataSetChanged();

//                                    mFolderList.onNoteEdit(note);

                                }


                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }
    public void showConfirmDeleteDialog(final Context context, String message, final Folder parentFolder, final Tab tab){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);
        alertDialogBuilder
                .setCancelable(false)
                .setTitle("Delete Tab "+message)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {

                               boolean deleted =  mDataApi.deleteTab(context, new DataApi.DataApiCallBack() {
                                    @Override
                                    public void onError(String error, Exception e) {
                                        showMessage(error);

                                    }
                                },tab.getName(),parentFolder);

                               if(deleted ==true){
                                   showMessage(String.format("%s %s %s","Tab",tab.getName()," deleted successfully"));


                                       if(currentPage>0){
                                           currentPage--;
                                       }
                                       mTabList.remove(tab);
                                       mSectionsPagerAdapter.setTabList(mTabList);

                                       mViewPager.setAdapter(null);
                                       mViewPager.setAdapter(mSectionsPagerAdapter);
                                       tabLayout.setupWithViewPager(mViewPager);
                                       mViewPager.setCurrentItem(currentPage);
                                       if(mTabList.size()>0) {
                                           noTabsTextView.setVisibility(View.GONE);
                                       }else{
                                           noTabsTextView.setVisibility(View.VISIBLE);
                                       }
                                       return;

                                  /* if(mTabList.size()==1) {
                                       mTabList.remove(tab);
                                       mSectionsPagerAdapter.setTabList(mTabList);

                                       mViewPager.setAdapter(null);
                                       mViewPager.setAdapter(mSectionsPagerAdapter);
                                       tabLayout.setupWithViewPager(mViewPager);
                                   }else {
                                       mTabList.remove(tab);

                                       mSectionsPagerAdapter.setTabList(mTabList);
                                       if (mTabList.size() == 1) {
                                           currentPage = 0;
                                       }else{
                                           if(currentPage>0){
                                               currentPage--;
                                           }
                                       }
                                       mSectionsPagerAdapter.notifyDataSetChanged();
                                       if (mTabList.size() == 0) {
                                           noTabsTextView.setVisibility(View.VISIBLE);


                                       }
                                   }*/



                               }

                                // get user input and set it to result
                                // edit text


                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }

    @Override
    public void onNoteEdit(Note note) {

        mCurrentFragment.onNoteDelete(note);
    }

    @Override
    public void onNoteDelete(Note note) {
        mCurrentFragment.onNoteDelete(note);

    }

    @Override
    public void onNoteOpened(Note note) {
        mFab.setVisibility(View.GONE);
    }

    @Override
    public void onNoteUiClosed(Note note) {
        mFab.setVisibility(View.VISIBLE);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class TabFragment extends Fragment implements NoteInteractionListener {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private long tabId,folderId;
        private String tabName,folderName;
        private  Tab mTab;
        private Folder mParentFolder;
        private DataApi mDataApi;
        private RecyclerView mRecyclerView;
        private TextView mNoNotesTextView;
        private List<Note> mNoteList;
        private NoteInteractionListener mNoteInteractionListener;
       private NoteRecyclerViewAdapter mnNoteRecyclerViewAdapter;


        public TabFragment() {

        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            if (getArguments() != null) {
                tabId= getArguments().getLong(ARG_TAB_ID);
                tabName = getArguments().getString(ARG_TAB_NAME);
                folderId= getArguments().getLong(ARG_FOLDER_ID);
                folderName = getArguments().getString(ARG_FOLDER_NAME);
            }
            if(tabName ==null || folderName ==null) {

                showError(DataApi.ErrorMessages.ERROR_REQUIRED_TAB_NAME, null);
                return;
            }
            mDataApi = NotesDataApi.newInstance(getContext());
            mParentFolder = new Folder(folderId,folderName);

            mTab = new Tab(mParentFolder,tabId,tabName);
            final Iterable<Note>  noteIterable =    mDataApi.getNotes(new DataApi.DataApiCallBack() {

                @Override
                public void onError(String error, Exception e) {

                    showError(error,e);

                }
            },mTab);
            mNoteList =new ArrayList<>();

            if(noteIterable!=null ) {
                Iterator<Note> noteIterator=  noteIterable.iterator();
                while (noteIterator.hasNext()) {
                    Note note =noteIterator.next();
                    mNoteList.add(note);

                }

            }
            mnNoteRecyclerViewAdapter = new NoteRecyclerViewAdapter(getContext(),mNoteList,this);



        }
        private void reFillNotes(Tab tab){
            final Iterable<Note>  noteIterable =    mDataApi.getNotes(new DataApi.DataApiCallBack() {

                @Override
                public void onError(String error, Exception e) {

                    showError(error,e);

                }
            },tab);
            mNoteList =new ArrayList<>();

            if(noteIterable!=null ) {
                Iterator<Note> noteIterator=  noteIterable.iterator();
                while (noteIterator.hasNext()) {
                    Note note =noteIterator.next();
                    mNoteList.add(note);

                }

            }
            mnNoteRecyclerViewAdapter = new NoteRecyclerViewAdapter(getContext(),mNoteList,this);
            if(mRecyclerView!=null){
                mRecyclerView.setAdapter(mnNoteRecyclerViewAdapter);
            }
        }



        public void clear(){
            mNoNotesTextView.setVisibility(View.GONE);
        }
        public boolean addNote(Note note){
             mNoNotesTextView.setVisibility(View.GONE);
            return mnNoteRecyclerViewAdapter.addItem(note);

        }

        private void showError(String error, Object o) {
            Toast.makeText(getContext(),error,Toast.LENGTH_LONG).show();
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         * @param tab
         */
        public static TabFragment newInstance(Tab tab,String parentFolderName, long parentFolderId) {
            TabFragment fragment = new TabFragment();
            Bundle args = new Bundle();
            args.putLong(ARG_TAB_ID, tab.getId());
            args.putString(ARG_TAB_NAME,tab.getName());
            args.putLong(ARG_FOLDER_ID,parentFolderId);
            args.putString(ARG_FOLDER_NAME,parentFolderName);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_tab_parent, container, false);
            if(mnNoteRecyclerViewAdapter.getItemCount()>0){
                rootView.findViewById(R.id.no_notes).setVisibility(View.GONE);
            }
            mRecyclerView = rootView.findViewById(R.id.list);
            mRecyclerView.setAdapter(mnNoteRecyclerViewAdapter);
            mNoNotesTextView = rootView.findViewById(R.id.no_notes);
            return rootView;
        }


        @Override
        public void onNoteEdit(Note note) {

        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            if(context instanceof NoteInteractionListener){
                mNoteInteractionListener = (NoteInteractionListener) context;
            }
        }

        @Override
        public void onNoteDelete(Note note) {
            mNoNotesTextView.setVisibility(View.VISIBLE);

            mnNoteRecyclerViewAdapter.removeItem(note);

        }

        @Override
        public void onNoteUiClosed(Note note) {
            if(mNoteInteractionListener!=null){
                mNoteInteractionListener.onNoteUiClosed(note);
            }
        }

        @Override
        public void onNoteOpened(Note note) {
            if(mNoteInteractionListener!=null){
                mNoteInteractionListener.onNoteOpened(note);
            }
            if(note!=null){


                FragmentManager fragmentManager =    getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                NoteFragment noteFragment =NoteFragment.newInstance(note.getName(),tabName,note.getUri());
                fragmentTransaction.add(R.id.content_tab, noteFragment,NoteFragment.class.getSimpleName());
                fragmentTransaction.addToBackStack(NoteFragment.class.getSimpleName());
                fragmentTransaction.commit();

            }

        }
    }

    @Override
    public void onBackPressed() {



        getSupportActionBar().setTitle(""+folderName);
        super.onBackPressed();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public  class SectionsPagerAdapter extends FragmentPagerAdapter {


        List<Tab> mTabList;

        public SectionsPagerAdapter(FragmentManager fm, List<Tab> tabList) {
            super(fm);
            mTabList =tabList;
        }

        public void setTabList(List<Tab> mTabList) {
            this.mTabList = mTabList;
            notifyDataSetChanged();
        }

        public Tab getTab(int position){
            return mTabList.get(position);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            TabFragment fragment = (TabFragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;
        }
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }
        public Fragment getRegisteredFragment(int position) {
            return registeredFragments.get(position);
        }


        @Override
        public TabFragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a TabFragment (defined as a static inner class below).
            return TabFragment.newInstance(mTabList.get(position),parentFolder.getName(),parentFolder.getFolderId());
        }

        @Override
        public long getItemId(int position) {
            return mTabList.get(position).getId();
        }

        @Override
        public int getCount() {
            return mTabList.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mTabList.get(position).getName();
        }
    }

    public void showCreateTabDialog(final Folder parentFolder, final DataApi.DataApiCallBack dataApiCallBack) {

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.name_prompt_dialog, null);
        final TextView nameTextView = promptsView.findViewById(R.id.textView1);
        nameTextView.setText("Enter a name for the new tab");

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = promptsView
                .findViewById(R.id.editTextDialogUserInput);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {

                                // get user input and set it to result
                                // edit text
                                if (userInput.getText().toString().trim().length() == 0) {
                                    showMessage("Enter a name");
                                    return;

                                }
                                Tab tab =  mDataApi.createTab(TabParentActivity.this,dataApiCallBack, parentFolder,userInput.getText().toString().trim());

                                if(tab!=null) {
                                    mTabList.add(tab);
                                    showMessage("Created tab " + tab.getName() + " successfully");

                                    noTabsTextView.setVisibility(View.GONE);


//                                mSectionsPagerAdapter.setTabList(mTabList);
//                                mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),mTabList);
//                                mViewPager.setAdapter(mSectionsPagerAdapter);

                                    mSectionsPagerAdapter.notifyDataSetChanged();
                                    for(int i=currentPage;i<mTabList.size();i++) {
                                        mViewPager.setCurrentItem(i, true);

                                    }
                                }

                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }
}
