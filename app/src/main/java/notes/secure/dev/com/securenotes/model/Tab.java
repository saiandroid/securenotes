package notes.secure.dev.com.securenotes.model;

public class Tab  extends  Item{

    private long id;
    private String name;
    private  Folder parentFolder;


    public  Tab(Folder parentFolder,long id, String name){
      this.parentFolder =parentFolder;
      this.id =id;
      this.name =name;
    }
    public Tab(Folder parentFolder){
        this.parentFolder =parentFolder;
    }




    public String getName() {
        return name;
    }

    public long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Folder getParentFolder() {
        return parentFolder;
    }

    public void setParentFolder(Folder parentFolder) {
        this.parentFolder = parentFolder;
    }
}
