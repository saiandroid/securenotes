package notes.secure.dev.com.securenotes.folder;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import notes.secure.dev.com.securenotes.R;
import notes.secure.dev.com.securenotes.api.data.DataApi;
import notes.secure.dev.com.securenotes.api.data.NotesDataApi;
import notes.secure.dev.com.securenotes.folder.FolderFragment.OnListFragmentInteractionListener;
import notes.secure.dev.com.securenotes.model.Folder;
import notes.secure.dev.com.securenotes.model.Tab;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Folder} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class FolderRecyclerViewAdapter extends RecyclerView.Adapter<FolderRecyclerViewAdapter.ViewHolder> {

    private  List<Folder> mFolderList;
    private final OnListFragmentInteractionListener mListener;
    private DataApi mDataApi;
    private Context mContext;
    private OnDataModifiedListener mOnDataModifiedListener;

    public FolderRecyclerViewAdapter(Context context, List<Folder> items, OnListFragmentInteractionListener listener,
                                     FolderRecyclerViewAdapter.OnDataModifiedListener mOnDataModifiedListener) {
        mFolderList = items;
        mListener = listener;
        mDataApi = NotesDataApi.newInstance(context);
        mContext =context;
        this.mOnDataModifiedListener =mOnDataModifiedListener;
    }

    public static interface OnDataModifiedListener{
        void updateUi();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.folder_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mFolderList.get(position);
        holder.mContentView.setText(mFolderList.get(position).getName());

        setDisplayInfo(holder, position);
         holder.mDeleteImageView.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 handleDeleteFolder(holder);
             }
         });

        holder.mEditImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showEditFolderDialog(holder.mItem);
            }
        });


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);

                }
            }
        });
    }
    public void showEditFolderDialog(final Folder oldFolder) {

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(mContext);
        View promptsView = li.inflate(R.layout.name_prompt_dialog, null);
        final TextView nameTextView = promptsView.findViewById(R.id.textView1);
        nameTextView.setText("Enter new name");

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                mContext);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = promptsView
                .findViewById(R.id.editTextDialogUserInput);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {

                                // get user input and set it to result
                                // edit text
                                if (userInput.getText().toString().trim().length() == 0) {
                                    showMessage("Enter a name");
                                    return;

                                }
                                Folder folder =   mDataApi.updateFolder(mContext, new DataApi.DataApiCallBack() {
                                    @Override
                                    public void onError(String error, Exception e) {
                                        showMessage(error);

                                    }
                                },userInput.getText().toString(),oldFolder.getName());
                                if(folder!=null){
                                    int ind =  mFolderList.indexOf(oldFolder);

                                    mFolderList.add(ind,folder);
                                    mFolderList.remove(oldFolder);
                                    showMessage("Folder renamed successfully");
                                    fillTabData(folder);
                                    notifyDataSetChanged();

//                                    mFolderList.onNoteEdit(note);

                                }


                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    private void fillTabData(Folder folder){
        Iterable<Tab> tabIterable =    mDataApi.getTabsOfFolder(new DataApi.DataApiCallBack() {
            @Override
            public void onError(String error, Exception e) {

            }
        },folder);
        if(tabIterable!=null){
            Iterator<Tab> tabIterator = tabIterable.iterator();
            List<Tab> tabList = new ArrayList<>();
            while(tabIterator.hasNext()){
                tabList.add(tabIterator.next());
            }
            folder.setTabList(tabList);
        }
    }

    private void handleDeleteFolder(ViewHolder holder) {

        showConfirmDialog(mContext,"Delete folder \""+holder.mItem.getName() +"\", all tabs and notes inside will be deleted ?",holder.mItem);

    }

    private void showMessage(String error) {
        Toast.makeText(mContext,error,Toast.LENGTH_LONG).show();
    }


    private void setDisplayInfo(ViewHolder holder, int position) {
        long timeCreated = mFolderList.get(position).getDateCreated();
        String display ="";

        if(mFolderList.get(position).getTabList()!=null){
            List<Tab> tabList = mFolderList.get(position).getTabList();
            if(tabList.size() ==0){
                display = "No tabs";

            }else if(tabList.size()==1){
                display = String.format("%d %s",tabList.size(),"tab");

            } else{
                display = String.format("%d %s",tabList.size(),"tabs");

            }
        }

        if(timeCreated!=0){
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM YYYY");
            try {
                String date = simpleDateFormat.format(new Date(timeCreated));
                if(display.length()>0){
                    display = String.format("%s, created %s",display,date);
                }else{
                    display = String.format("created %s",date);
                }
//                String.format("%s ")
            }catch (Exception e){
                e.printStackTrace();
            }

        }
        holder.mIdView.setText(display);
    }

    @Override
    public int getItemCount() {
        return mFolderList.size();
    }

    public void setFolderList(List<Folder> mFolderList) {
        this.mFolderList =mFolderList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public final ImageView mDeleteImageView,mEditImageView;
        public Folder mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.item_number);
            mContentView = (TextView) view.findViewById(R.id.content);
            mDeleteImageView = view.findViewById(R.id.deleteFolder);
            mEditImageView = view.findViewById(R.id.editFolder);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }

    public void showConfirmDialog(Context context, String message,final Folder folder){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);
        alertDialogBuilder
                .setCancelable(false)
                .setTitle(message)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {

                                boolean deleted = mDataApi.deleteFolder(mContext, new DataApi.DataApiCallBack() {
                                    @Override
                                    public void onError(String error, Exception e) {
                                        showMessage(error);

                                    }
                                },folder.getName());
                                if(deleted){
                                    showMessage("Folder deleted successfully");
                                   int ind =   mFolderList.indexOf(folder);
                                    mFolderList.remove(ind);
                                    FolderRecyclerViewAdapter.this.notifyItemRemoved(ind);

                                    mOnDataModifiedListener.updateUi();

                                }

                                // get user input and set it to result
                                // edit text


                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }
}
