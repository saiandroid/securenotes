package notes.secure.dev.com.securenotes.folder.tab.note;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import notes.secure.dev.com.securenotes.Constants;
import notes.secure.dev.com.securenotes.Crypto;
import notes.secure.dev.com.securenotes.R;
import notes.secure.dev.com.securenotes.api.data.DataApi;
import notes.secure.dev.com.securenotes.api.data.NotesDataApi;
import notes.secure.dev.com.securenotes.folder.FolderFragment;
import notes.secure.dev.com.securenotes.model.Note;

public class NoteFragment extends Fragment {

    private DataApi mDataApi;
    private String mNoteName;
    private String mTabName,mNoteUri;
    private Note mNote;


    public static NoteFragment newInstance(String noteName,String tabName,String noteUri) {

        Bundle args = new Bundle();
        args.putString(Constants.ARG_TAB_NAME,tabName);
        args.putString(Constants.ARG_NOTE_NAME,noteName);
        args.putString(Constants.ARG_NOTE_URI,noteUri);


        NoteFragment fragment = new NoteFragment();
        fragment.setArguments(args);
        return fragment;
    }
   private NoteInteractionListener mListener;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof NoteInteractionListener) {
            mListener = (NoteInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mDataApi = NotesDataApi.newInstance(getContext());

        if(getArguments()!=null){
           mNoteName =  getArguments().getString(Constants.ARG_NOTE_NAME);
           mTabName =  getArguments().getString(Constants.ARG_TAB_NAME);
            mNoteUri =  getArguments().getString(Constants.ARG_NOTE_URI);

        }
        if(mNoteName ==null || mTabName ==null || mNoteUri ==null){
            showMessage ("Error note name, uri and tab name are required");
            throw new IllegalArgumentException("note name  and  tab name are required");

        }

        mNote = new Note();
        mNote.setUri(mNoteUri);
        mNote.setName(mNoteName);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_note_details,menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() ==R.id.action_edit_note){
            noteEditTextView.setEnabled(true);
        }else if(item.getItemId() ==R.id.action_save_note){
            if(noteEditTextView.isEnabled() ==true) {

               String content =  noteEditTextView.getText().toString();

                   String encyptedContent = null;

                   try {
                       encyptedContent = Crypto.encryptContent(Crypto.ALGOS[Crypto.ALGO_INDEX], Crypto.ALGOS_KEYS[Crypto.ALGO_INDEX], content, new DataApi.DataApiCallBack() {
                           @Override
                           public void onError(String error, Exception e) {
                               showMessage(error);

                           }
                       });
                   }catch (Exception e){
                       showMessage(e.toString());
                   }
                   if (encyptedContent == null) {
                       return true;
                   }
              Note noteSaved =   mDataApi.saveNoteContent(getContext(), new DataApi.DataApiCallBack() {
                    @Override
                    public void onError(String error, Exception e) {
                        showMessage(error);

                    }
                }, mNoteUri, mNoteName, encyptedContent);

              if(noteSaved!=null){
                  noteEditTextView.setText(noteSaved.getContent());
                  showMessage("Note saved successfully");
                  mListener.onNoteUiClosed(mNote);
                  getActivity().onBackPressed();
              }
            }else{
                showMessage("You need to edit note first before saving");
            }
        }
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mListener!=null) {
            mListener.onNoteUiClosed(mNote);
        }

    }

    public void showNoteDeleteConfirmDialog(Context context, String message){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);
        alertDialogBuilder
                .setCancelable(false)
                .setTitle("Delete Note "+message)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {

                              boolean deleted =   mDataApi.deleteNote(getContext(), new DataApi.DataApiCallBack() {
                                    @Override
                                    public void onError(String error, Exception e) {
                                        showMessage(error);

                                    }
                                },mNoteUri);

                              if(deleted){
                                  showMessage("Note deleted successfully");
                                  mListener.onNoteDelete(mNote);

                              }

                                // get user input and set it to result
                                // edit text


                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }

    private void showMessage(String s) {
        Toast.makeText(getContext(),s,Toast.LENGTH_LONG).show();
    }
    TextView noteEditTextView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.note_view, container, false);
        ( (AppCompatActivity) getActivity()).getSupportActionBar().setTitle(mNoteName);
        noteEditTextView = view.findViewById(R.id.noteEditText);
       String noteContent =  mDataApi.getFileContent(getContext(), new DataApi.DataApiCallBack() {
            @Override
            public void onError(String error, Exception e) {
                showMessage(error);
            }
        },mNoteUri);
        if(noteContent!=null && noteContent.length()>0) {

            try{
                noteContent = Crypto.decryptContent(Crypto.ALGOS[Crypto.ALGO_INDEX], Crypto.ALGOS_KEYS[Crypto.ALGO_INDEX], noteContent, new DataApi.DataApiCallBack() {
                    @Override
                    public void onError(String error, Exception e) {
                        showMessage(e.toString());
                    }
                });


            }catch (Exception e){
                showMessage(e.toString());
            }

            noteEditTextView.setText(noteContent != null ? noteContent : "");
        }else{
            noteEditTextView.setEnabled(true);
        }
        return view;
    }
}
