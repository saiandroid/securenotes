package notes.secure.dev.com.securenotes.api.data;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import notes.secure.dev.com.securenotes.R;
import notes.secure.dev.com.securenotes.model.Folder;
import notes.secure.dev.com.securenotes.model.Note;
import notes.secure.dev.com.securenotes.model.Tab;


public class NotesDataApi extends DataApi {
    private final String PUBLIC_FOLDER_NAME;
    private Context context;
    public final static boolean useExternal  =false;

    public static NotesDataApi _INSTANCE_;
    private final String EXT =".txt";


    private NotesDataApi(Context context) {
        super(context);
        this.context = context;
        PUBLIC_FOLDER_NAME = context.getString(R.string.shared_folder_name);
        createFoldersIfRequired();
    }

    public static NotesDataApi newInstance(Context context) {


        if (_INSTANCE_ == null) {
            _INSTANCE_ = new NotesDataApi(context);
        }
        return _INSTANCE_;
    }


    @Override
    public Iterable<Note> getNotes(DataApiCallBack dataApiCallBack, Tab tab) {

        File tabFile = new File(getRootOfFolders(), tab.getParentFolder().getName());
       /* if( f.isDirectory() ){
            dataApiCallBack.onError(ErrorMessages.ERROR_FOLDER_DOES_NOT_EXIST,null);
            return Collections.EMPTY_LIST;
        }
*/

        File f = new File(tabFile, tab.getName());


        // Array of files in the images subdirectory
        File[] mPrivateFiles = f.listFiles();

        List<Note> noteList = new ArrayList<>();
        if (mPrivateFiles != null) {
            for (int i = 0; i < mPrivateFiles.length; i++) {
                Note note = new Note();
                File file = mPrivateFiles[i];
                String s = file.getName();
                s = s.substring(s.lastIndexOf(File.separator) + File.separator.length());//get last name with extension
                if(s.endsWith(EXT)){
                    s = s.substring(0,s.lastIndexOf(EXT));
                }
                note.setName(s);
//            dataFile.setUri(Uri.fromFile(file));
                note.setUri(file.getAbsolutePath());
                noteList.add(note);

            }
        }

        return noteList;

    }

    @Override
    public Iterable<Tab> getTabsOfFolder(DataApiCallBack dataApiCallBack, Folder folder) {

        File f = new File(getRootOfFolders(), folder.getName());
        if (f == null || f.exists() == false) {
            dataApiCallBack.onError(ErrorMessages.ERROR_FOLDER_DOES_NOT_EXIST, null);
            return Collections.EMPTY_LIST;
        }


        // Array of files in the images subdirectory
        File[] mPrivateFiles = f.listFiles();

        List<Tab> tabList = new ArrayList<>();
        for (int i = 0; i < mPrivateFiles.length; i++) {
            Tab tab = new Tab(folder);
            File file = mPrivateFiles[i];
            String s = file.getName();
            s = s.substring(s.lastIndexOf(File.separator) + File.separator.length());//get last name with extension

            tab.setName(s);

            long id = file.lastModified();
            if (id > 0) {
                tab.setId(id);
            } else {
                tab.setId(System.currentTimeMillis());
            }
//            dataFile.setUri(Uri.fromFile(file));
//            tab.setUri(file.getAbsolutePath());
            tabList.add(tab);

        }

        return tabList;
    }

    @Override
    public Iterable<Folder> getRootFolders(DataApiCallBack dataApiCallBack) {
        File f = getRootOfFolders();
        if (f == null || f.exists() == false) {
            dataApiCallBack.onError(ErrorMessages.ERROR_ROOT_FOLDER_DOES_NOT_EXIST, null);
            return Collections.EMPTY_LIST;
        }

        // Array of files in the images subdirectory
        File[] mPrivateFiles = f.listFiles();

        List<Folder> folderList = new ArrayList<>();
        for (int i = 0; i < mPrivateFiles.length; i++) {
            Folder folder = new Folder();
            File file = mPrivateFiles[i];
            String s = file.getName();
            s = s.substring(s.lastIndexOf(File.separator) + File.separator.length());//get last name with extension
            long id = file.lastModified();
            if (id > 0) {
                folder.setFolderId(id);
                folder.setDateCreated(id);

            } else {
                folder.setFolderId(System.currentTimeMillis());
                folder.setDateCreated(System.currentTimeMillis());

            }
            folder.setName(s);

//            dataFile.setUri(Uri.fromFile(file));
//            folder.setUri(file.getAbsolutePath());
            folderList.add(folder);

        }

        return folderList;
    }

    @Override
    public Folder createRootFolder(Context context, DataApiCallBack dataApiCallBack, String name) {
        createFoldersIfRequired();
        File f = getRootOfFolders();
        if (f == null || f.exists() == false) {
            dataApiCallBack.onError(ErrorMessages.ERROR_CREATING_FOLDER + name + ", as " + ErrorMessages.ERROR_ROOT_FOLDER_DOES_NOT_EXIST, null);
            return null;
        }
        File folderFile = new File(f, name);
        if (folderFile.exists() == false) {
            if (true == folderFile.mkdirs()) {
                Folder folder = new Folder();
                folder.setDateCreated(folderFile.lastModified() > 0 ? folderFile.lastModified() : System.currentTimeMillis());
                folder.setFolderId(folderFile.lastModified() > 0 ? folderFile.lastModified() : System.currentTimeMillis());
                folder.setName(name);
                return folder;
            }


        }

        dataApiCallBack.onError(ErrorMessages.ERROR_FOLDER_ALREADY_EXISTS, null);


        return null;
    }

    @Override
    public Tab createTab(Context context, DataApiCallBack dataApiCallBack, Folder parentFolder, String name) {
        createFoldersIfRequired();
        File f = getRootOfFolders();
        if (f == null || f.exists() == false) {
            dataApiCallBack.onError(ErrorMessages.ERROR_FOLDER_DOES_NOT_EXIST, null);

            return null;
        }
        File folderFile = new File(f, parentFolder.getName());
        if (folderFile.exists() == false) {
            dataApiCallBack.onError(ErrorMessages.ERROR_FOLDER_DOES_NOT_EXIST, null);

            return null;
        }

        File tabFolder = new File(folderFile, name);
        if (tabFolder.exists() == false) {
            if (tabFolder.mkdirs() == true) {
                Tab tab = new Tab(parentFolder, 0, name);
                tab.setId(tabFolder.lastModified() > 0 ? tabFolder.lastModified() : System.currentTimeMillis());
                return tab;
            } else {
                dataApiCallBack.onError(ErrorMessages.ERROR_CREATING_FOLDER + " " + name, null);

            }

        }
        dataApiCallBack.onError(ErrorMessages.ERROR_TAB_ALREADY_EXISTS, null);


        return null;
    }

    @Override
    public Note createNote(Context context, DataApiCallBack dataApiCallBack, Tab tab, String noteName) {
        createFoldersIfRequired();
        if(noteName.endsWith(EXT) ==false) {

            noteName = noteName + EXT;
        }
        File f = getRootOfFolders();
        if (f == null || f.exists() == false) {
            return null;
        }
        Folder parentFolder = tab.getParentFolder();

        File parentFolderFile = new File(f, parentFolder.getName());
        if (parentFolderFile.exists() == false) {
            dataApiCallBack.onError(ErrorMessages.ERROR_FOLDER_DOES_NOT_EXIST, null);
            return null;
        }

        File folderFile = new File(f, parentFolder.getName());
        if (folderFile.exists() == false) {
            dataApiCallBack.onError(ErrorMessages.ERROR_FOLDER_DOES_NOT_EXIST, null);
            return null;
        }

        File tabFolder = new File(folderFile, tab.getName());
        if (tabFolder.exists() == false) {
            dataApiCallBack.onError(ErrorMessages.ERROR_FOLDER_DOES_NOT_EXIST, null);
            return null;
        }

        Note note = new Note();
        File noteFile = new File(tabFolder, noteName);
        if (noteFile.exists() == true) {
            dataApiCallBack.onError(ErrorMessages.ERROR_NOTE_ALREADY_EXISTS, null);
            return null;
        }
        try {
            if (noteFile.createNewFile() == true) {
                if(noteName.endsWith(EXT)){
                    noteName = noteName.substring(0,noteName.lastIndexOf(EXT));
                }
                note.setName(noteName);
                note.setUri(noteFile.getAbsolutePath());


            } else {
                dataApiCallBack.onError(ErrorMessages.ERROR_CREATING_NOTE, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            dataApiCallBack.onError(ErrorMessages.ERROR_CREATING_NOTE, e);
        }
        return note;

    }

    @Override
    public boolean deleteNote(Context context, DataApiCallBack dataApiCallBack, String uri) {

        if (uri != null) {
            File file = new File(uri);
            if (file.delete() == true) {
                return true;
            }
            dataApiCallBack.onError(ErrorMessages.ERROR_DELETING_FILE, null);
        }


        return false;
    }

    @Override
    public Note updateNote(Context context, DataApiCallBack dataApiCallBack, String noteUri, String noteName, String oldName) {
        if(noteName.endsWith(EXT) ==false) {

            noteName = noteName + EXT;
        }

        if (noteUri != null) {
            Note note = null;
            File file = new File(noteUri);
            String root = file.getParent();
            File newFile = new File(root, noteName);

            if (file.exists() == true && newFile.length() == 0) {
                if (file.renameTo(newFile)) {
                    note = new Note();
                    if(noteName.endsWith(EXT)){
                        noteName = noteName.substring(0,noteName.lastIndexOf(EXT));
                    }
                    note.setName(noteName);
                    note.setUri(newFile.getAbsolutePath());
                }
                return note;
            }
            if (newFile.length() > 0) {
                dataApiCallBack.onError(ErrorMessages.ERROR_FIlE_ALREADY_EXISTS, null);
                return null;
            }
            dataApiCallBack.onError(ErrorMessages.ERROR_UPDATING_NOTE, null);
        }

        return null;
    }

    @Override
    public Folder updateFolder(Context context, DataApiCallBack dataApiCallBack, String folderName, String oldName) {
        File f = getRootOfFolders();
        if (f == null || f.exists() == false) {
            return null;
        }
        File folder  = new File(f,oldName);
        if(folder.exists() ==false){
            dataApiCallBack.onError(ErrorMessages.ERROR_FOLDER_DOES_NOT_EXIST+" "+oldName,null);
            return null;
        }

        try {
            if(new File(folder.getParent(),folderName).exists()==true){
                dataApiCallBack.onError(ErrorMessages.ERROR_FOLDER_ALREADY_EXISTS+" "+folderName,null);
                return null;
            }
            if (folder.renameTo(new File(folder.getParent(),folderName))) {
                Folder folder1 = new Folder();
                folder1.setName(folderName);
                folder1.setDateCreated(System.currentTimeMillis());
                folder1.setFolderId(System.currentTimeMillis());
                return folder1;
            }
        }catch (Exception e){
            dataApiCallBack.onError(ErrorMessages.ERROR_CREATING_FOLDER,e);
        }
        return null;
    }

    @Override
    public Tab updateTab(Context context, DataApiCallBack dataApiCallBack, Folder parentFolder, String name, String oldName) {
        File f = getRootOfFolders();
        if (f == null || f.exists() == false) {
            return null;
        }
        File folder  = new File(f,parentFolder.getName());
        if(folder.exists() ==false){
            dataApiCallBack.onError(ErrorMessages.ERROR_FOLDER_DOES_NOT_EXIST,null);
            return null;
        }

        File tab  = new File(folder,oldName);
        if(tab.exists() ==false){
            dataApiCallBack.onError(ErrorMessages.ERROR_DELETING_TAB+" "+oldName,null);
            return null;
        }

        try {
            if(new File(tab.getParent(),name).exists()==true){
                dataApiCallBack.onError(ErrorMessages.ERROR_TAB_ALREADY_EXISTS+" "+name,null);
                return null;
            }
            if (tab.renameTo(new File(tab.getParent(),name))) {
                Tab tab1 = new Tab(parentFolder,System.currentTimeMillis(),name);
                return tab1;
            }
        }catch (Exception e){
            dataApiCallBack.onError(ErrorMessages.ERROR_DELETING_TAB,e);
        }
        return null;
    }

    @Override
    public Note saveNoteContent(Context context, DataApiCallBack dataApiCallBack, String noteUri, String noteName, String content) {
        if (noteUri != null) {

            if(noteName.endsWith(EXT) ==false) {

                noteName = noteName + EXT;
            }

            Note note = null;
            File file = new File(noteUri);
            if (file.exists() == true) {
                try {
                    FileOutputStream fileOutputStream = new FileOutputStream(file, false);
                    writeContent(fileOutputStream, content);
                } catch (Exception e) {
                    dataApiCallBack.onError(e.toString(), e);
                    return null;

                }
                note = new Note();
                if(noteName.endsWith(EXT)){
                    noteName = noteName.substring(0,noteName.lastIndexOf(EXT));
                }
                note.setName(noteName);
                note.setUri(noteUri);
                note.setContent(content);


                return note;
            }
            dataApiCallBack.onError(ErrorMessages.ERROR_DELETING_FILE, null);
        }


        return null;
    }

    private void writeContent(FileOutputStream fileOutputStream, String content) throws IOException {

        fileOutputStream.write(content.getBytes());

    }

    @Override
    public String getFileContent(Context context, DataApiCallBack dataApiCallBack, String uri) {

        File file = new File(uri);
        if (file.exists() == true) {
            try {
                return readContent(file);

            } catch (Exception e) {
                e.printStackTrace();
                dataApiCallBack.onError(ErrorMessages.ERROR_READING_FILE, e);
            }
            return null;
        }
        return null;
    }

    @Override
    public boolean updateFileContent(Context context, DataApiCallBack dataApiCallBack, String uri, String content) {


        return false;
    }


    private File getRootOfFolders() {
        File f = new File(context.getFilesDir(), PUBLIC_FOLDER_NAME);

        if (f.exists() == true) {
            return f;
        }

        if (useExternal == true && isExternalStorageWritable() == true) {

            f = new File(Environment.getExternalStorageDirectory(), PUBLIC_FOLDER_NAME);


            if (f.exists() == false) {
                return null;
            } else {
            /*if(f.isDirectory()){
                f.delete();
            }else{
                f.delete();
            }*/

            }

            return f;
        }

        return null;


    }


    private boolean createFoldersIfRequired() {

        //if it already exists on internal storage just return it
        File f = getRootOfFolders();

        if (f != null) {
            return true;
        }

        //checks first time if external storage is unavailable we will create folder structure internally

        if (useExternal == false || isExternalStorageWritable() == false) {

            f = new File(context.getFilesDir(), PUBLIC_FOLDER_NAME);


            if (f.exists() == false) {
                if (false == f.mkdirs()) {
                    return false;
                } else {

                }
            }

        }
        f = new File(Environment.getExternalStorageDirectory(), PUBLIC_FOLDER_NAME);

        if (f.exists() == false) {

            //might require permissions
            if (false == f.mkdirs()) {
                return false;
            } else {
                System.out.print("Created dirs ");
            }
        }


        return true;
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteFolder(Context context, DataApiCallBack dataApiCallBack, String name) {
        File f = getRootOfFolders();
        if (f == null || f.exists() == false) {
            dataApiCallBack.onError(ErrorMessages.ERROR_ROOT_FOLDER_DOES_NOT_EXIST, null);
            return false;
        }
        File folderFile = new File(f, name);
        if (folderFile == null || folderFile.exists() == false) {
            dataApiCallBack.onError(ErrorMessages.ERROR_FOLDER_DOES_NOT_EXIST, null);
            return false;
        }
        File[] mPrivateFiles = folderFile.listFiles();

        for (int i = 0; i < mPrivateFiles.length; i++) {
            File file = new File(folderFile, mPrivateFiles[i].getName());
            if(false ==deleteTab(context,dataApiCallBack,mPrivateFiles[i].getName(),new Folder(folderFile.lastModified(),name))) {
//                if (file.delete() == false) {
//                    dataApiCallBack.onError(ErrorMessages.ERROR_DELETING_FILE + " " + file.getName(), null);
                    return false;
//                }
            }


        }
        if (folderFile.delete() == false) {
            dataApiCallBack.onError(ErrorMessages.ERROR_DELETING_FOLDER + " " + folderFile.getName(), null);
            return false;
        }
        return true;
    }

    @Override
    public boolean deleteTab(Context context, DataApiCallBack dataApiCallBack, String name, Folder parentFolder) {

        File f = getRootOfFolders();
        if (f == null || f.exists() == false) {
            dataApiCallBack.onError(ErrorMessages.ERROR_ROOT_FOLDER_DOES_NOT_EXIST, null);
            return false;
        }
        File folderFile = new File(f, parentFolder.getName());
        if (folderFile == null || folderFile.exists() == false) {
            dataApiCallBack.onError(ErrorMessages.ERROR_FOLDER_DOES_NOT_EXIST, null);
            return false;
        }
        File tabFile = new File(folderFile, name);
        if (tabFile == null || tabFile.exists() == false) {
            dataApiCallBack.onError(ErrorMessages.ERROR_FOLDER_DOES_NOT_EXIST, null);
            return false;
        }
        File[] mPrivateFiles = tabFile.listFiles();

        if (mPrivateFiles != null) {
            for (int i = 0; i < mPrivateFiles.length; i++) {
                File file = new File(tabFile, mPrivateFiles[i].getName());
                if (file.delete() == false) {
                    dataApiCallBack.onError(ErrorMessages.ERROR_DELETING_FILE + " " + file.getName(), null);
                    return false;
                }
            }
        }
        if (tabFile.delete() == false) {
            dataApiCallBack.onError(ErrorMessages.ERROR_DELETING_TAB + " " + name, null);
            return false;
        }
        return true;
    }


    private String readContent(File file) {
        try {
            BufferedReader fileReader = new BufferedReader(new FileReader(file));
            String line = null;
            StringBuilder sb = new StringBuilder();
            while ((line = fileReader.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();

        } catch (Exception e) {

        }


        return null;
    }
}
