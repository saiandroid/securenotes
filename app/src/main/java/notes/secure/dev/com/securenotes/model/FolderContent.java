package notes.secure.dev.com.securenotes.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class FolderContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<Folder> FOLDERS = new ArrayList();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<Long, Folder> ITEM_MAP = new HashMap();

    private static final int COUNT = 25;

    static {
        // Add some sample items.
        for (int i = 1; i <= COUNT; i++) {
            addItem(createDummyItem(i));
        }
    }

    private static void addItem(Folder item) {
        FOLDERS.add(item);
        ITEM_MAP.put(item.getFolderId(), item);
    }

    private static Folder createDummyItem(int position) {
        return new Folder((position), "Item " + position);
    }

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore details information here.");
        }
        return builder.toString();
    }

}
