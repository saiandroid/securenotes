package notes.secure.dev.com.securenotes.model;

import java.util.List;

public class Folder extends  Item {

    private String name;
    private  long folderId,dateCreated,dateModified;
    List<Tab> tabList;


    public Folder(long folderId, String s) {

        this.folderId =folderId;
        this.name =s;
    }

    public  Folder(){

    }

    public String getName() {
        return name;
    }

    public List<Tab> getTabList() {
        return tabList;
    }

    public long getDateCreated() {
        return dateCreated;
    }

    public long getDateModified() {
        return dateModified;
    }

    public long getFolderId() {
        return folderId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDateCreated(long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public void setFolderId(long folderId) {
        this.folderId = folderId;
    }

    public void setDateModified(long dateModified) {
        this.dateModified = dateModified;
    }

    public void setTabList(List<Tab> tabList) {
        this.tabList = tabList;
    }
}
