package notes.secure.dev.com.securenotes.api.data;

import android.content.Context;

import notes.secure.dev.com.securenotes.model.Item;
import notes.secure.dev.com.securenotes.model.Note;
import notes.secure.dev.com.securenotes.model.Folder;
import notes.secure.dev.com.securenotes.model.Tab;

/**
 */

public abstract class DataApi {

    public static interface DataApiCallBack{


        void onError(String error ,Exception e);
    }

    public static interface ErrorMessages{

        public final String ERROR_ROOT_FOLDER_DOES_NOT_EXIST ="Root folder does not exists";

        public final String ERROR_FOLDER_DOES_NOT_EXIST ="Folder does not exists";
        public final String ERROR_FOLDER_ALREADY_EXISTS ="Folder already  exists";
        String ERROR_CREATING_FOLDER ="error creating folder" ;
        String ERROR_NOTE_ALREADY_EXISTS = "error note already exists" ;
        String ERROR_CREATING_NOTE ="error creating note" ;
        String ERROR_REQUIRED_TAB_NAME ="error parent tab name is required" ;
        String ERROR_DELETING_FILE = "error deleting file";
        String ERROR_DELETING_FOLDER = "error deleting folder";
        String ERROR_DELETING_TAB="error deleting tab";
        String ERROR_TAB_ALREADY_EXISTS ="Tab already  exists" ;
        String ERROR_UPDATING_NOTE = "error updating note";
        String ERROR_FIlE_ALREADY_EXISTS ="error, file already exists";
        String ERROR_READING_FILE = "error reading file";
    }




    public DataApi(Context context){

    }
    //get all files inside a given tab
    public abstract Iterable<Note> getNotes(DataApiCallBack dataApiCallBack ,Tab tab);
    //get all folders inside root
    public abstract Iterable<Folder> getRootFolders(DataApiCallBack dataApiCallBack);

    public abstract Iterable<Tab> getTabsOfFolder(DataApiCallBack dataApiCallBack,Folder folder);

    //create a note inside a given tab
    public abstract Note createNote(Context context,DataApiCallBack dataApiCallBack, Tab tab, String noteName);

    //delete a note
    public abstract boolean  deleteNote(Context context, DataApiCallBack dataApiCallBack,String uri);

    //delete a note
    public abstract boolean  deleteFolder(Context context, DataApiCallBack dataApiCallBack,String name);
    //delete a note
    public abstract boolean  deleteTab(Context context, DataApiCallBack dataApiCallBack,String name,Folder parentFolder);

// it will handle encryption  decryption
    public abstract String getFileContent(Context context,DataApiCallBack dataApiCallBack, String uri);

    public abstract boolean updateFileContent(Context context, DataApiCallBack dataApiCallBack,String uri,String content);

    public abstract Folder createRootFolder(Context context,DataApiCallBack dataApiCallBack ,String name);

    public abstract Tab createTab(Context context ,DataApiCallBack dataApiCallBack,Folder parentFolder , String name);

    public abstract Note saveNoteContent(Context context,DataApiCallBack dataApiCallBack, String noteUri, String noteName,String content);

    public abstract Note updateNote(Context context,DataApiCallBack dataApiCallBack, String noteUri, String noteName,String oldName);

    public abstract Folder updateFolder(Context context,DataApiCallBack dataApiCallBack, String folderName,String oldName);

    public abstract Tab updateTab(Context context,DataApiCallBack dataApiCallBack, Folder parentFolder,String name,String oldName);

}
